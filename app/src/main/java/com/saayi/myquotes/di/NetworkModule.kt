package com.saayi.myquotes.di

import com.saayi.myquotes.network.ApiService
import com.saayi.myquotes.network.RetrofitHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {
    @Provides
    fun providesApiService(): ApiService {
        return RetrofitHelper.getInstance().create(ApiService::class.java)
    }
}