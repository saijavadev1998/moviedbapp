package com.saayi.myquotes.di

import android.content.Context
import com.saayi.myquotes.local.AppDatabase
import com.saayi.myquotes.local.PopularMovieDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(@ApplicationContext context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

    @Provides
    fun providePopularMoviesDao(appDatabase: AppDatabase): PopularMovieDao {
        return appDatabase.popularMoviesDao()
    }
}