package com.saayi.myquotes.network

import com.saayi.myquotes.BuildConfig
import com.saayi.myquotes.network.model.PopularMoviesModel
import com.saayi.myquotes.utils.AppConstants.POPULAR_MOVIES_END_POINT
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET(POPULAR_MOVIES_END_POINT)
    fun getAllPopularMovies(
        @Query("api_key") apiKey: String = BuildConfig.API_KEY,
        @Query("page") page: Int = 1,
    ): Call<PopularMoviesModel>
}