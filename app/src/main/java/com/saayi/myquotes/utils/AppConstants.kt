package com.saayi.myquotes.utils

object AppConstants {
    const val APP_DATABASE_NAME = "appDatabase"
    const val POPULAR_MOVIES_TABLE = "popularMoviesTable"
    const val POPULAR_MOVIES_END_POINT = "popular"
    const val POPULAR_MOVIES = "Popular Movies"
}