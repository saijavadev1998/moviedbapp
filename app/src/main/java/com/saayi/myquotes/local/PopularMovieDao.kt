package com.saayi.myquotes.local

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import com.saayi.myquotes.local.model.PopularMoviesInfo
import kotlinx.coroutines.flow.Flow

@Dao
interface PopularMovieDao {

    @Upsert
    suspend fun insertPopularMovie(popularMovie: PopularMoviesInfo): Long

    @Query("SELECT * FROM popularMoviesTable")
    fun getAllData(): Flow<List<PopularMoviesInfo>>
}