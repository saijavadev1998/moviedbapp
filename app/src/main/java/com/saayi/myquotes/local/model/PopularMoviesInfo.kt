package com.saayi.myquotes.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.saayi.myquotes.utils.AppConstants.POPULAR_MOVIES_TABLE

@Entity(tableName = POPULAR_MOVIES_TABLE)
data class PopularMoviesInfo(
    @ColumnInfo("adult")
    val adult: Boolean,
    @ColumnInfo("backdropPath")
    val backdropPath: String,
    @ColumnInfo("genreIds")
    val genreIds: List<Long>,
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo("id")
    val id: Long,
    @ColumnInfo("originalLanguage")
    val originalLanguage: String,
    @ColumnInfo("originalTitle")
    val originalTitle: String,
    @ColumnInfo("overview")
    val overview: String,
    @ColumnInfo("popularity")
    val popularity: Double,
    @ColumnInfo("posterPath")
    val posterPath: String,
    @ColumnInfo("releaseDate")
    val releaseDate: String,
    @ColumnInfo("title")
    val title: String,
    @ColumnInfo("video")
    val video: Boolean,
    @ColumnInfo("voteAverage")
    val voteAverage: Double,
    @ColumnInfo("voteCount")
    val voteCount: Long,
)