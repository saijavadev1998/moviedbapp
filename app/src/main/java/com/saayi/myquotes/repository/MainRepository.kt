package com.saayi.myquotes.repository

import android.util.Log
import com.saayi.myquotes.local.PopularMovieDao
import com.saayi.myquotes.local.model.PopularMoviesInfo
import com.saayi.myquotes.network.ApiService
import com.saayi.myquotes.network.model.PopularMoviesModel
import kotlinx.coroutines.flow.Flow
import retrofit2.Call
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val apiService: ApiService,
    private val popularMovieDao: PopularMovieDao
) {
    val TAG = "MainRepository"

    fun fetchPopularMovies(): Call<PopularMoviesModel> {
        Log.d(TAG, "Fetching all popular movies from API")
        return apiService.getAllPopularMovies()
    }

    suspend fun savePopularMovies(movie: PopularMoviesInfo) {
        Log.d(TAG, "Popular movie inserted in repository")
        popularMovieDao.insertPopularMovie(movie)
    }

    fun getAllPopularMovies(): Flow<List<PopularMoviesInfo>> {
        return popularMovieDao.getAllData()
    }
}