package com.saayi.myquotes.activities.ui.interactions

import com.saayi.myquotes.local.model.PopularMoviesInfo
import com.saayi.myquotes.utils.ViewEvent
import com.saayi.myquotes.utils.ViewState

sealed class PopularMoviesMVI {
    sealed class PopularMoviesUIEvent : ViewEvent {
        data object LoadPopularMovies : PopularMoviesUIEvent()
    }

    data class PopularMoviesDataState(
        val isLoading: Boolean,
        val popularMovies: List<PopularMoviesInfo>? = null,
    ) : ViewState
}