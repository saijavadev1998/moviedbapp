package com.saayi.myquotes.activities.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.saayi.myquotes.activities.ui.interactions.PopularMoviesMVI
import com.saayi.myquotes.local.model.PopularMoviesInfo
import com.saayi.myquotes.network.model.PopularMoviesModel
import com.saayi.myquotes.network.model.Result
import com.saayi.myquotes.repository.MainRepository
import com.saayi.myquotes.utils.ViewEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.Call
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: MainRepository,
) : ViewModel() {

    init {
        getPopularMovies()
    }

    val TAG = "MainViewModel"
    private val _dataState: MutableStateFlow<PopularMoviesMVI.PopularMoviesDataState> =
        MutableStateFlow(PopularMoviesMVI.PopularMoviesDataState(isLoading = true))
    val dataState = _dataState.asStateFlow()

    fun handleEvents(event: ViewEvent) {
        when (event) {
            PopularMoviesMVI.PopularMoviesUIEvent.LoadPopularMovies -> {
                fetchAllPopularMovies()
            }
        }
    }

    private fun fetchAllPopularMovies() {
        viewModelScope.launch(Dispatchers.IO) {
            val call: Call<PopularMoviesModel> = repository.fetchPopularMovies()
            try {
                val response = call.execute()
                if (response.isSuccessful) {
                    val data = response.body()
                    Log.d(TAG, "API call successful data: $data")
                    data?.results?.forEach { popularMovie ->
                        savePopularMovie(
                            movie = popularMoviesMapper(popularMovie)
                        )
                    }
                } else {
                    Log.d(TAG, "API call failed")
                }
            } catch (e: Exception) {
                Log.d(TAG, "Exception while API call: $e")
            }
        }
    }

    private suspend fun savePopularMovie(movie: PopularMoviesInfo) {
        repository.savePopularMovies(movie = movie)
    }

    private fun getPopularMovies() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getAllPopularMovies().collect { movies ->
                _dataState.value = _dataState.value.copy(
                    isLoading = false,
                    popularMovies = movies,
                )
            }
        }
    }

    private fun popularMoviesMapper(asset: Result): PopularMoviesInfo {
        return PopularMoviesInfo(
            adult = asset.adult,
            backdropPath = asset.backdropPath,
            genreIds = asset.genreIds,
            id = asset.id,
            originalLanguage = asset.originalLanguage,
            originalTitle = asset.originalTitle,
            overview = asset.overview,
            popularity = asset.popularity,
            posterPath = asset.posterPath,
            releaseDate = asset.releaseDate,
            title = asset.title,
            video = asset.video,
            voteAverage = asset.voteAverage,
            voteCount = asset.voteCount,
        )
    }
}