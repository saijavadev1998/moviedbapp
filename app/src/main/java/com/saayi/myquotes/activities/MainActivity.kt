package com.saayi.myquotes.activities

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.saayi.myquotes.BuildConfig
import com.saayi.myquotes.activities.ui.MainViewModel
import com.saayi.myquotes.activities.ui.interactions.PopularMoviesMVI
import com.saayi.myquotes.ui.theme.MyQuotesTheme
import com.saayi.myquotes.utils.AppConstants.POPULAR_MOVIES
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyQuotesTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainScreen()
                }
            }
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun MainScreen() {
    val viewModel: MainViewModel = hiltViewModel()
    val uiState = viewModel.dataState.collectAsState()
    LaunchedEffect(key1 = Unit, block = {
        viewModel.handleEvents(PopularMoviesMVI.PopularMoviesUIEvent.LoadPopularMovies)
    })
    when {
        uiState.value.isLoading -> {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = Color(0xFF8EB8E4)),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(
                    color = Color.White,
                    strokeWidth = 2.dp
                )
            }
        }

        else -> {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = Color(0xFF000000))
            ) {
                Column {
                    Text(
                        modifier = Modifier
                            .padding(vertical = 20.dp),
                        text = POPULAR_MOVIES,
                        fontSize = 20.sp,
                        fontWeight = FontWeight.W900
                    )
                    LazyRow(
                        horizontalArrangement = Arrangement.spacedBy(20.dp)
                    ) {
                        uiState.value.popularMovies?.let {
                            items(it.size) { index ->
                                Box(
                                    modifier = Modifier
                                        .background(
                                            color = Color.Gray,
                                            shape = RoundedCornerShape(20.dp)
                                        )
                                        .wrapContentHeight()
                                        .width(200.dp)
                                ) {
                                    Column(
                                        modifier = Modifier
                                            .padding(8.dp),
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        verticalArrangement = Arrangement.spacedBy(10.dp)
                                    ) {
                                        val imageUrl =
                                            BuildConfig.IMAGE_BASE_URL + uiState.value.popularMovies?.getOrNull(
                                                index
                                            )?.posterPath
                                        val title = uiState.value.popularMovies?.getOrNull(
                                            index
                                        )?.title
                                        Card(
                                            modifier = Modifier
                                                .height(250.dp)
                                                .width(200.dp)
                                        ) {
                                            GlideImage(
                                                modifier = Modifier,
                                                model = imageUrl,
                                                contentDescription = null,
                                                contentScale = ContentScale.Crop
                                            )
                                        }
                                        Text(
                                            modifier = Modifier,
                                            text = title ?: "",
                                            fontSize = 12.sp,
                                            fontWeight = FontWeight.W500,
                                            maxLines = 2,
                                            textAlign = TextAlign.Center
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}